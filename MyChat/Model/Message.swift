//
//  Message.swift
//  MyChat
//
//  Created by Hen Joy on 5/21/19.
//

import UIKit
@objcMembers
class Message: NSObject {
    var sender: String?
    var recipient: String?
    var message: String?
    var created: Date?
}
