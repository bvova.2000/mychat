//
//  User.swift
//  MyChat
//
//  Created by Hen Joy on 5/21/19.
//

import UIKit

class User: NSObject {
    var name : String?
    var email : String?
    var urlImage : String?
    
    static func makeUrlForImage(string str: String) -> String{
        let result = "https://backendlessappcontent.com/\(APPLICATION_ID)/\(API_KEY)/files/\(str)"
        return result
    }
    
    override init() {}
    
    init(name : String?, email: String?, urlImage: String?) {
        self.name = name
        self.email = email
        self.urlImage = urlImage
    }
    
}
