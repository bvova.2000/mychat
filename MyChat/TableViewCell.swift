//
//  TableViewCell.swift
//  MyChat
//
//  Created by Hen Joy on 5/13/19.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var tableVCell: UIImageView!
    @IBOutlet weak var labelLogin: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
