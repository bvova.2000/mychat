//
//  UsersVC.swift
//  MyChat
//
//  Created by Hen Joy on 5/13/19.
//

import UIKit
import SDWebImage

class UsersVC: UITableViewController {
    
    var allUsers = [User]()
    var stateOfQuery = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let UNib = UINib(nibName: "ProfileTableVCell", bundle: nil)
        tableView.register(UNib, forCellReuseIdentifier: "ProfileTableVCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !stateOfQuery {
            makeAQueryToDB()
        }
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = currentUserName
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allUsers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableVCell", for: indexPath) as! ProfileTableVCell
        cell.profileImageView!.sd_setImage(with: URL(string: allUsers[indexPath.row].urlImage!), completed: nil)
        cell.emailLabel.text = allUsers[indexPath.row].email!
        cell.nameLabel.text = allUsers[indexPath.row].name!
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ProfileTableVCell
        
        recipientEmail = cell.emailLabel.text!
        recipientName = cell.nameLabel.text!
        self.performSegue(withIdentifier: "ChatSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChatSegue" {
            let chatVC = segue.destination as! ChatVC
            if let recipImageIndex = allUsers.firstIndex(where: { (user) -> Bool in
                user.name == recipientName
            }){
                chatVC.recipientUserImage = allUsers[recipImageIndex].urlImage
            }
            if let senderImageIndex = backendless.userService.currentUser.getProperty("image"){
                chatVC.currentUserImage = User.makeUrlForImage(string: (senderImageIndex as! String))
            }
        }
    }
    
    func makeAQueryToDB(){
        let whereClause = "Login != '\(currentUserName)'"
        let queryBuilder = DataQueryBuilder()
        queryBuilder!.setWhereClause(whereClause)
        let dataStore = backendless.data.ofTable("Users")
        dataStore?.find(queryBuilder,
                        response: {
                            (foundUsers) -> () in
                            let allUsers = foundUsers as! [[String : Any]]
                            self.stateOfQuery = true
                            self.fillAllData(users: allUsers)
        },
                        error: {
                            (fault : Fault?) -> () in
                            print("Server reported an error: \(String(describing: fault))")
        })
    }
    
    func fillAllData(users: [[String : Any]]){
        for user in users {
            let url = User.makeUrlForImage(string: user["image"] as! String)
            let userExample = User()
            userExample.urlImage = url
            userExample.name = user["Login"] as? String
            userExample.email = user["email"] as? String
            allUsers.append(userExample)
            tableView.reloadData()
        }
    }
    
    
}
