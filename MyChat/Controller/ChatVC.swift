//
//  ChatVC.swift
//  MyChat
//
//  Created by Hen Joy on 5/20/19.
//

import UIKit
import SDWebImage

var recipientEmail = ""
var recipientName = ""

class ChatVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var chatScrollView: UIScrollView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var chatTextView: UITextView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet var scrollViewTapGesture: UITapGestureRecognizer!
    
    var chatMessages = [Message]()
    var currentUserImage: String?
    var recipientUserImage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatTextView.addSubview(placeholderLabel)
        self.title = recipientName
                updateChat()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        let url = "https://backendlessappcontent.com/EBDB3A3B-68DC-ADF2-FF95-6B0B46C73A00/15C8772A-7CCB-FCFE-FF7E-AA9B921D5900/files/Images/IMG_1557749662.909966.png"
        //        SDWebImageManager.shared.loadImage(with: URL(string: url), options: [], progress: nil, completed: { (image, data, error, cacheType, success, url) in
        //            DispatchQueue.main.async {
        //                guard let image = image else {
        //                    return
        //                }
        //                self.chatScrollView.backgroundColor = UIColor(patternImage: image)
        //            }
        //        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = chatTextView.hasText ? true : false
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
            placeholderLabel.isHidden = chatTextView.hasText ? true : false
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        if chatTextView.hasText{
            return
        }
        var dict = notification.userInfo!
        let keyBoardSize = dict.removeValue(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let frameKeyboardSize = keyBoardSize.cgRectValue
        UIView.animate(withDuration: 0.3, animations: {
            self.chatScrollView.frame.size.height -= frameKeyboardSize.height
            self.chatTextView.frame.origin.y -= frameKeyboardSize.height
//            let scrollViewOffset: CGPoint = CGPoint(x: 0, y: self.chatScrollView.contentSize.height - self.chatScrollView.bounds.height)
//            self.chatScrollView.setContentOffset(scrollViewOffset, animated: true)
        }, completion: nil)
        
    }
    
    @objc func keyboardWillHide(notification: Notification){
        var dict = notification.userInfo!
        let keyBoardSize = dict.removeValue(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let frameKeyboardSize = keyBoardSize.cgRectValue
        UIView.animate(withDuration: 0.3, animations: {
            self.chatScrollView.frame.size.height += frameKeyboardSize.height
            self.chatTextView.frame.origin.y += frameKeyboardSize.height
        }, completion: nil)
    }
    
    func updateChat() {
        let messageMarginX = CGFloat(40)
        var messageMarginY = CGFloat(27)
        let bubbleMarginX: CGFloat = 30
        var bubleMarginY: CGFloat = 16
        
        let imageMarginX: CGFloat = 10
        var imageMarginY: CGFloat = 5
        chatMessages.removeAll(keepingCapacity: false)
        chatMessages = loadChat()
        for i in 0..<chatMessages.count {
            
            let messageLabel = UILabel()
            let bubbleLabel = UILabel()
            let senderImage = UIImageView()
            messageLabel.text = chatMessages[i].message
            messageLabel.frame = CGRect(x: 0, y: 0, width: chatScrollView.frame.size.width - 90, height: CGFloat.maximum(0, 0))
            
            messageLabel.numberOfLines = 0
            messageLabel.lineBreakMode = .byWordWrapping
            messageLabel.sizeToFit()
            messageLabel.textAlignment = .left
            messageLabel.font = UIFont(name: "Apple SD Gothic Neo", size: 16)
            
            bubbleLabel.frame.size = CGSize(width: messageLabel.frame.size.width + 20, height: messageLabel.frame.size.height + 20)
            senderImage.frame.size = CGSize(width: 35, height: 35)
            
            if chatMessages[i].sender == currentUserName {
                messageLabel.textColor = .black
                messageLabel.frame.origin.x = chatScrollView.frame.size.width - messageMarginX - messageLabel.frame.size.width
                bubbleLabel.frame.origin.x = chatScrollView.frame.size.width -  bubbleLabel.frame.size.width - bubbleMarginX
                bubbleLabel.backgroundColor = UIColor(red: 0, green: 250, blue: 0, alpha: 1)
                senderImage.frame.origin = CGPoint(x: chatScrollView.frame.size.width - senderImage.frame.size.width - imageMarginX, y: imageMarginY)
                senderImage.sd_setImage(with: URL(string: currentUserImage!), completed: nil)
            }else {
                messageLabel.textColor = .white
                messageLabel.frame.origin.x = messageMarginX
                bubbleLabel.frame.origin.x = bubbleMarginX
                bubbleLabel.backgroundColor = .blue
                senderImage.frame.origin = CGPoint(x: imageMarginX, y: imageMarginY)
                senderImage.sd_setImage(with: URL(string: recipientUserImage!), completed: nil)
            }
            bubbleLabel.frame.origin.y = bubleMarginY
            bubleMarginY += bubbleLabel.frame.size.height + 10
            bubbleLabel.layer.cornerRadius = 20
            bubbleLabel.clipsToBounds = true
            chatScrollView.addSubview(bubbleLabel)
            
            messageLabel.frame.origin.y = messageMarginY
            messageMarginY += messageLabel.frame.size.height + 30
            chatScrollView.addSubview(messageLabel)
            
            senderImage.layer.borderWidth = 1
            senderImage.layer.borderColor = UIColor.gray.cgColor
            senderImage.layer.cornerRadius = senderImage.frame.size.width / 2
            senderImage.clipsToBounds = true
            chatScrollView.addSubview(senderImage)
            imageMarginY += bubbleLabel.frame.size.height + 10
            let width = view.frame.size.width
            chatScrollView.contentSize = CGSize(width: width, height: messageMarginY)
            let scrollViewOffset: CGPoint = CGPoint(x: 0, y: chatScrollView.contentSize.height - chatScrollView.bounds.height)
            chatScrollView.setContentOffset(scrollViewOffset, animated: true)
        }
    }
    
    func saveMessage(mes: String) {
        let message = Message()
        message.created = Date()
        message.message = mes
        message.recipient = recipientName
        message.sender = currentUserName
        let dataStore = backendless.data.of(Message.ofClass())
        dataStore!.save(message,
                        response: {
                            (message) -> () in
                            print("message saved")
                            self.chatTextView.text = ""
                            self.placeholderLabel.isHidden = false
                            self.updateChat()
                            
        },
                        error: {
                            (fault : Fault?) -> () in
                            print("Server reported an error: \(String(describing: fault))")
        })
    }
    
    func loadChat() -> [Message] {
        let messages = downloadMessage(recip: recipientName, send: currentUserName)
        let messages2 = downloadMessage(recip: currentUserName, send: recipientName)
        var result = messages + messages2
        result.sort(by: { $0.created! < $1.created! })
        return result
    }
    
    func downloadMessage(recip: String, send: String) -> [Message]{
        let whereClause = "recipient = '\(recip)' AND sender = '\(send)'"
        let queryBuilder = DataQueryBuilder()
        queryBuilder!.setWhereClause(whereClause)
        let dataStore = backendless.data.of(Message().ofClass())
        var error: Fault?
        let result = dataStore?.find(queryBuilder) as? [Message]
        if result == nil {
            print("Server reported an error: \(error)")
        }
        return result!
    }
    
    @IBAction func sendMessageButton(_ sender: UIButton) {
        view.endEditing(true)
        if chatTextView.text.isEmpty{
            return
        }
        saveMessage(mes: chatTextView.text!)
    }
    
    @IBAction func tapGestureHideKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
}
