//
//  SignUpVC.swift
//  MyChat
//
//  Created by Hen Joy on 5/10/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var LoginTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            imageView.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        EmailTextField.resignFirstResponder()
        PasswordTextField.resignFirstResponder()
        LoginTextField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let mainViewHeight = self.view.bounds.height
        let mainViewWidth = self.view.bounds.width
        var pointsUp = 0.0 as CGFloat
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: { () -> Void in
            if UIScreen.main.bounds.height <= 568 {
                if textField == self.EmailTextField {
                    pointsUp = 50
                }else
                    if textField == self.LoginTextField {
                        pointsUp = 80
                    }else
                        if textField == self.PasswordTextField {
                            pointsUp = 130
                }
            }
            self.view.center = CGPoint(x: mainViewWidth / 2 , y: mainViewHeight / 2 - pointsUp)
        }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let mainViewHeight = self.view.bounds.height
        let mainViewWidth = self.view.bounds.width
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.view.center = CGPoint(x: mainViewWidth / 2, y: mainViewHeight / 2)
        }, completion: nil)
    }
    
    @IBAction func AddImageButtonPressed(_ sender: UIButton) {
        let MyImagePicker = UIImagePickerController()
        MyImagePicker.delegate = self
        MyImagePicker.sourceType = .photoLibrary
        MyImagePicker.allowsEditing = true
        self.present(MyImagePicker, animated: true, completion: nil)
    }
    
    @IBAction func SignUpButtonPressed(_ sender: UIButton) {
        let imageData = self.imageView.image!.pngData()
        let fileName="IMG_\(Date().timeIntervalSince1970).png";
        let filePath = "Images/\(fileName)"
        backendless.file.saveFile(filePath, content: imageData, overwriteIfExist: true)
        let alert = UIAlertView()
        alert.title = "Error"
        var errorMessage = ""
        let user = BackendlessUser()
        user.setProperty("image", object: filePath)
        if EmailTextField.text != nil {
            user.email = NSString(string: EmailTextField.text!)
        }
        else {
            errorMessage = "To write correct email"
        }
        if LoginTextField.text != nil {
            user.setProperty("Login", object: LoginTextField.text)
        }
        else {
            errorMessage = errorMessage != "" ? "To write correct login" : errorMessage + ", login"
        }
        if PasswordTextField.text != nil {
            user.password = NSString(utf8String: PasswordTextField.text!)
        }
        else {
            errorMessage = errorMessage != "" ? "To write correct password" : errorMessage + ", password"
        }
        if errorMessage != "" {
            alert.message = errorMessage
            alert.show()
            return
        }
        backendless.userService.register(user, response: {
            (registeredUser : BackendlessUser?) -> Void in
            print("User registered \(String(describing: registeredUser?.value(forKey: "email")))")
            currentUserName = user.getProperty("Login") as! String
            self.performSegue(withIdentifier: "ToUsersSegue2", sender: self)
        }, error: {
            (fault : Fault?) -> Void in
            print("Server reported an error: \(String(describing: fault?.description))")
        })
        
    }
    
    
}
