//
//  ViewController.swift
//  MyChat
//
//  Created by Hen Joy on 5/10/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

var currentUserName = ""
let APPLICATION_ID = "EBDB3A3B-68DC-ADF2-FF95-6B0B46C73A00"
let API_KEY = "15C8772A-7CCB-FCFE-FF7E-AA9B921D5900"
let SERVER_URL = "https://api.backendless.com"
let backendless = Backendless.sharedInstance()!

class LoginViewController: UIViewController {
    
    @IBOutlet weak var LoginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        backendless.hostURL = SERVER_URL
        backendless.initApp(APPLICATION_ID, apiKey: API_KEY)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func SignInButton(_ sender: UIButton) {
        do{
            backendless.userService.login(LoginTextField.text, password: passwordTextField.text)
            guard let user = backendless.userService.currentUser else{
                print("Not registered")
                return
            }
            currentUserName = user.getProperty("Login") as! String
            self.performSegue(withIdentifier: "ToUsersSegue1", sender: self)
        } catch let error as NSError{
            print("sign up failed due to error: \(error.localizedDescription)")
        }
        
        
    }
    
    @IBAction func NewAccountButton(_ sender: UIButton) {
    }
}


